import HttpService from './http-service';
import Tag from '../models/tag';
import HttpResponse from '../models/http-response';

const TagService = {
  getTags: async (): Promise<HttpResponse<Tag[]>> => {
    const req: Request = new Request('http://localhost:8081/api/v2/tags/all');
    return await HttpService.request(req);
  },

  createTags: async (tags: Tag[]): Promise<HttpResponse<Tag[]>> => {
    const req: Request = new Request(
      'http://localhost:8081/api/v2/tags/create-all',
      {
        method: 'POST',
        body: JSON.stringify(tags),
        headers: { 'Content-Type': 'application/json' },
      }
    );
    return await HttpService.request(req);
  },
};

export default TagService;
