import HttpService from './http-service';
import { Todo } from '../models/todo';
import HttpResponse from '../models/http-response';

const TodoService = {
  async getAllTodos(): Promise<HttpResponse<Todo[]>> {
    return await HttpService.request('http://localhost:8081/api/v2/todos/get');
  },

  async getTodoById(id: string): Promise<HttpResponse<Todo>> {
    return await HttpService.request(
      `http://localhost:8081/api/v2/todos/get/${id}`
    );
  },

  async createTodo(todo: Todo): Promise<HttpResponse<Todo>> {
    const req: Request = new Request('http://localhost:8081/api/v2/todos/new', {
      method: 'POST',
      body: JSON.stringify(todo),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return await HttpService.request(req);
  },

  async updateTodo(todo: Todo): Promise<HttpResponse<Todo>> {
    const req: Request = new Request(
      'http://localhost:8081/api/v2/todos/update',
      {
        method: 'PUT',
        body: JSON.stringify(todo),
        headers: { 'Content-Type': 'application/json' },
      }
    );
    return await HttpService.request(req);
  },

  async deleteTodoById(id: string): Promise<HttpResponse<null>> {
    const req: Request = new Request(
      `http://localhost:8081/api/v2/todos/delete/${id}`,
      { method: 'DELETE', headers: { 'Content-Type': 'application/json' } }
    );
    return await HttpService.request(req);
  },
};

export default TodoService;
