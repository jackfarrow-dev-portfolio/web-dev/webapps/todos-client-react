import HttpResponse from '../models/http-response';

const HttpService = {
  async request<T>(req: string | Request): Promise<HttpResponse<T>> {
    let request: Request | null = null;
    if (req instanceof String) {
      request = new Request(req, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
    }
    const response = await fetch(request || req);
    const { status, ok, url } = response;
    const json = await response.json();

    if (!response.ok) {
      const { message, zonedDateTime } = json;
      return {
        status,
        message,
        timestamp: zonedDateTime,
        url,
        ok,
      };
    }
    return {
      status,
      ok,
      url,
      data: json.data as T,
    };
  },
};

export default HttpService;
