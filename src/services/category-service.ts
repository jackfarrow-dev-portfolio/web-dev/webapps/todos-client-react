import { Category } from '../models/category';
import HttpResponse from '../models/http-response';
import HttpService from './http-service';

const CategoryService = {
  fetchCategories: async (): Promise<
    HttpResponse<{ categories: Category[] }>
  > => {
    const req = new Request('http://localhost:8081/api/v2/categories/all');
    return await HttpService.request(req);
  },
};

export default CategoryService;
