import { useContext, useEffect } from 'react';
import { HttpStatusContext } from '../../store/http-status-context';
import classes from './HttpStatusMessage.module.css';
import classNames from 'classnames';

const HttpStatusMessage: React.FC = () => {
  const httpStatusCtx = useContext(HttpStatusContext);
  const { reqStatusCode, reqType, reqErrorMsg, todoTitle } = httpStatusCtx;
  useEffect(() => {
    const dismissalId = setTimeout(() => {
      httpStatusCtx.handleResetHttpStatus();
    }, 11000);
    return function () {
      clearTimeout(dismissalId);
    };
  }, []);

  const buildOutputMessage = (): string => {
    let outputMessage = '';
    if (httpStatusCtx.showHttpStatusMsg) {
      if (reqStatusCode > 399) {
        outputMessage = `Failed to ${reqType} todo "${todoTitle}": ${reqErrorMsg}`;
      } else {
        let operation = '';
        switch (reqType) {
          case 'POST':
            operation = 'Created';
            break;
          case 'PUT':
            operation = 'Updated';
            break;
          case 'DELETE':
            operation = 'Deleted';
            break;
          default:
            operation = 'Unknown Operation';
        }
        outputMessage = `${operation} todo ${todoTitle}.`;
      }
    }
    return outputMessage;
  };
  return (
    <div className={classes['http-status-message']}>
      <div
        className={
          reqStatusCode < 399
            ? classNames({
                [classes['status-message-container']]: true,
                [classes['http-success']]: true,
              })
            : classNames({
                [classes['status-message-container']]: true,
                [classes['http-failure']]: true,
              })
        }
      >
        <div className={classes['status-header-container']}>
          <h3>
            {reqStatusCode < 399 && reqErrorMsg === ''
              ? 'Success!'
              : 'An Error Occurred...'}
            <span className={classes['status-dismiss']}>
              <button onClick={httpStatusCtx.handleResetHttpStatus}>
                Close
              </button>
            </span>
          </h3>
        </div>
        <div className={classes['status-message']}>
          <p>{buildOutputMessage()}</p>
        </div>
      </div>
    </div>
  );
};

export default HttpStatusMessage;
