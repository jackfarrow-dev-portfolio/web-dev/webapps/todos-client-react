import { useContext, useEffect, useState } from 'react';
import { HttpStatusContext } from '../../store/http-status-context';
import { TodoContext } from '../../store/todo-context';
import { TrayContext } from '../../store/tray-context';
import TodoService from '../../services/todo-service';
import { Todo } from '../../models/todo';
import {
  parseDisplayDate,
  parseDisplayStatus,
} from '../../utils/parse-functions';
import classNames from 'classnames';
import classes from './DeleteTodo.module.css';

const DeleteConfirmation: React.FC = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const httpStatusCtx = useContext(HttpStatusContext);
  const todoCtx = useContext(TodoContext);
  const trayCtx = useContext(TrayContext);

  const todo: Todo = todoCtx.currentTodo
    ? todoCtx.currentTodo
    : {
        id: '',
        title: '',
        description: '',
        status: 'SUSPENDED',
        createDate: '',
      };

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setIsLoading(false);
    }, 125);

    return function () {
      clearTimeout(timeoutId);
    };
  }, []);

  const deleteTodo = async () => {
    const response = await TodoService.deleteTodoById(todo.id!);
    const httpStatus = {
      reqType: 'DELETE',
      reqStatusCode: response.status,
      reqUrl: response.url,
      isReqOk: response.ok,
      reqErrorMsg: response.message,
      todoTitle: todo.title,
      todoId: todo.id || '',
      showHttpStatusMsg: true,
    };
    if (response.ok) {
      todoCtx.handleRemoveTodo(todo.id!);
    }
    httpStatusCtx.handleSetHttpStatus(httpStatus);

    trayCtx.setModalMode({
      isModalOpen: false,
      isCreateMode: false,
      isDeleteMode: false,
      isEditMode: false,
    });
  };

  return (
    <div
      className={
        isLoading
          ? classNames({
              [classes['delete-confirmation']]: true,
              'float-up-fade-in': false,
            })
          : classNames({
              [classes['delete-confirmation']]: true,
              'float-up-fade-in': true,
            })
      }
    >
      <div className={classes['delete-confirmation-title']}>
        <h2 className="form-title">
          Delete "<span>{todo.title}</span>"?
        </h2>
        <p>Are you sure you want to delete this todo?</p>
      </div>
      <div className={classes['delete-confirmation-card']}>
        <ul>
          <li>Title: {todo.title}</li>
          <li>Description: {todo.description}</li>
          <li>Status: {parseDisplayStatus(todo.status)}</li>
          <li>Created On: {parseDisplayDate(todo.createDate)}</li>
        </ul>
      </div>
      <div
        id={classes['delete-confirmation-btns']}
        className="form-controls-container"
      >
        <button
          type="button"
          onClick={() => {
            trayCtx.setModalMode({
              isCreateMode: false,
              isDeleteMode: false,
              isEditMode: false,
              isModalOpen: false,
            });
            // toggleBodyOverflow(false);
            todoCtx.handleSetCurrentTodo(null);
          }}
        >
          Cancel
        </button>
        <button type="button" onClick={deleteTodo}>
          Delete
        </button>
      </div>
    </div>
  );
};

export default DeleteConfirmation;
