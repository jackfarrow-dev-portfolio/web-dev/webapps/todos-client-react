import { useContext } from 'react';
import { TrayContext } from '../../store/tray-context';
import { toggleBodyOverflow } from '../../utils/dom-handlers';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAnglesRight, faPlus } from '@fortawesome/free-solid-svg-icons';
import classes from './AppHeader.module.css';

const AppHeader: React.FC<{ title: string }> = ({ title }) => {
  const trayCtx = useContext(TrayContext);

  return (
    <nav className={classes['app-header']}>
      <button
        className={classes['open-tray']}
        onClick={() => {
          toggleBodyOverflow(true);
          trayCtx.handleToggleTray(true);
        }}
      >
        <FontAwesomeIcon icon={faAnglesRight} size="xl" />
      </button>
      <h1>{title}</h1>

      <div
        className={classes['create-todo']}
        onClick={() => {
          trayCtx.setModalMode({
            isModalOpen: true,
            isCreateMode: true,
            isEditMode: false,
            isDeleteMode: false,
          });
        }}
      >
        <FontAwesomeIcon icon={faPlus} size="xl" />
      </div>
    </nav>
  );
};

export default AppHeader;
