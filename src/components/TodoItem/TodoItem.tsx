import React, { useContext } from 'react';
import { Todo } from '../../models/todo';
import { TrayContext } from '../../store/tray-context';
import { TodoContext } from '../../store/todo-context';
import {
  parseDisplayDate,
  parseDisplayStatus,
  parseIconFromCategory,
} from '../../utils/parse-functions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestion } from '@fortawesome/free-solid-svg-icons';
import classes from './TodoItem.module.css';
import { Category } from '../../models/category';

const TodoItem: React.FC<{ todo: Todo }> = (props) => {
  const { title, description, createDate, status, categoryId, lastUpdatedOn } =
    props.todo;
  const trayCtx = useContext(TrayContext);
  const todoCtx = useContext(TodoContext);

  const categoryName =
    todoCtx.todoCategories.find((c: Category) => c.categoryId === categoryId)
      ?.categoryName || 'Unknown Category';

  const iconData = parseIconFromCategory(categoryName);
  return (
    <div className={classes['todo-item']}>
      <div
        className={classes['item-icon']}
        style={{ backgroundColor: iconData.color }}
      >
        <div className={classes.icon}>
          <FontAwesomeIcon
            icon={iconData.icon || faQuestion}
            size="5x"
            style={{ color: '#f6efef' }}
          />
        </div>
        <div className={classes['category-name']}>
          <p>{categoryName}</p>
        </div>
      </div>
      <div className={classes['todo-content']}>
        <h3>{title}</h3>
        <ul>
          <li>{description}</li>
          <li>
            <span>Status:</span> {parseDisplayStatus(status)}
          </li>
          <li>
            <span>Created On:</span> {parseDisplayDate(createDate)}
          </li>
          <li>
            <span>
              Last Updated:{' '}
              {lastUpdatedOn ? lastUpdatedOn : parseDisplayDate(createDate)}
            </span>
          </li>
        </ul>
        <div className={classes['todo-item-controls']}>
          <button
            type="button"
            className="action-button"
            onClick={() => {
              trayCtx.setModalMode({
                isEditMode: true,
                isModalOpen: true,
                isCreateMode: false,
                isDeleteMode: false,
              });
              todoCtx.handleSetCurrentTodo(props.todo);
            }}
          >
            Edit
          </button>
          <button
            type="button"
            className="cancel-button"
            onClick={() => {
              trayCtx.setModalMode({
                isEditMode: false,
                isModalOpen: true,
                isCreateMode: false,
                isDeleteMode: true,
              });
              todoCtx.handleSetCurrentTodo(props.todo);
            }}
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  );
};

export default TodoItem;
