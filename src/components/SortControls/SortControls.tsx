import { useContext, useState } from 'react';
import { TodoContext } from '../../store/todo-context';
import { TrayContext } from '../../store/tray-context';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCalendar,
  faFilter,
  faSort,
} from '@fortawesome/free-solid-svg-icons';
import { SORT_DIRS, STATUS_FILTERS } from '../../utils/sort-directions';
import { convertStatusToInputValue } from '../../utils/parse-functions';
import useSortFilterInput from '../../hooks/use-sort-filter-input';
import classes from './SortControls.module.css';

const { titleAZ, titleZA, dateAZ, dateZA } = SORT_DIRS;
const { complete, inProgress, notStarted, suspended } = STATUS_FILTERS;

const isNotAfterDate = (date: string, endDate: string): boolean =>
  Date.parse(date) <= Date.parse(endDate);

const isNotBeforeDate = (date: string, startDate: string): boolean =>
  Date.parse(date) >= Date.parse(startDate);

const SortControls: React.FC = () => {
  const todoCtx = useContext(TodoContext);
  const trayCtx = useContext(TrayContext);

  const [filterByStartDate, setFilterByStartDate] = useState<string>('');
  const [filterByEndDate, setFilterByEndDate] = useState<string>('');
  const [formHasErrors, setFormHasErrors] = useState<boolean>(false);
  const {
    sortValue,
    handleSortValueChange,
    filterValue,
    handleFilterValueChange,
  } = useSortFilterInput();

  const submitForm = () => {
    setFormHasErrors(false);
    if (sortValue.length) {
      todoCtx.handleSortTodos(sortValue);
    }

    todoCtx.handleFilterTodos({
      filters: filterValue,
      sortDirection: sortValue,
    });

    todoCtx.handleFilterTodosByDateRange(
      filterByStartDate,
      filterByEndDate,
      sortValue
    );

    trayCtx.handleToggleTray(false);
  };

  const submitHandler = (e: any) => {
    e.preventDefault();
    // Don't submit if start date set but not end date,
    // or end date set but not start date
    if (
      (filterByStartDate && !filterByEndDate) ||
      (filterByEndDate && !filterByStartDate)
    ) {
      setFormHasErrors(true);
      return;
    }
    // If start date and end date
    else if (filterByStartDate && filterByEndDate) {
      // Don't submit if start date AFTER end date,
      // or if end date BEFORE start date
      if (
        !isNotBeforeDate(filterByEndDate, filterByStartDate) ||
        !isNotAfterDate(filterByStartDate, filterByEndDate)
      ) {
        setFormHasErrors(true);
        return;
      } else {
        submitForm();
      }
    }

    // If not start date and not end date
    submitForm();
  };

  return (
    <form
      className={classes['sort-form']}
      onSubmit={(e: React.FormEvent<HTMLFormElement>) => {
        submitHandler(e);
      }}
    >
      <div className={classes['sort-container']}>
        <h3 className={classes['sort-icon']}>
          <span>
            <FontAwesomeIcon icon={faSort} color="#f6efef" size="1x" />
          </span>
          Sort By:
        </h3>
        <div className="form-radio-container">
          <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handleSortValueChange(e)
            }
            type="radio"
            name="sortBy"
            id={`sort-by-${titleAZ}`}
            value={titleAZ}
            defaultChecked={todoCtx.currentSortDirection === titleAZ}
          />
          <label htmlFor={`sort-by-${titleAZ}`}>Title A-Z</label>
        </div>
        <div className="form-radio-container">
          <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handleSortValueChange(e)
            }
            type="radio"
            name="sortBy"
            id={`sort-by-${titleZA}`}
            value={titleZA}
            defaultChecked={todoCtx.currentSortDirection === titleZA}
          />
          <label htmlFor={`sort-by-${titleZA}`}>Title Z-A</label>
        </div>
        <div className="form-radio-container">
          <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handleSortValueChange(e)
            }
            type="radio"
            name="sortBy"
            id={`sort-by-${dateAZ}`}
            value={dateAZ}
            defaultChecked={todoCtx.currentSortDirection === dateAZ}
          />
          <label htmlFor={`sort-by-${dateAZ}`}>Date A-Z</label>
        </div>
        <div className="form-radio-container">
          <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handleSortValueChange(e)
            }
            type="radio"
            name="sortBy"
            id={`sort-by-${dateZA}`}
            value={dateZA}
            defaultChecked={todoCtx.currentSortDirection === dateZA}
          />
          <label htmlFor={`sort-by-${dateZA}`}>Date Z-A</label>
        </div>
      </div>

      <div className={classes['sort-container']}>
        <h3 className={classes['sort-icon']}>
          <span>
            <FontAwesomeIcon icon={faFilter} color="#f6efef" size="1x" />
          </span>
          Filter By Status:
        </h3>
        <div className="form-checkbox-container">
          <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handleFilterValueChange(e)
            }
            type="checkbox"
            name="filterByStatus"
            id={`filter-by-status-${complete}`}
            value={convertStatusToInputValue(complete)}
            defaultChecked={todoCtx.currentFilters.includes(
              convertStatusToInputValue(complete)
            )}
          />
          <label htmlFor={`filter-by-status-${complete}`}>Complete</label>
        </div>
        <div className="form-checkbox-container">
          <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handleFilterValueChange(e)
            }
            type="checkbox"
            name="filterByStatus"
            id={`filter-by-status-${inProgress}`}
            value={convertStatusToInputValue(inProgress)}
            defaultChecked={todoCtx.currentFilters.includes(
              convertStatusToInputValue(inProgress)
            )}
          />
          <label htmlFor={`filter-by-status-${inProgress}`}>In Progress</label>
        </div>
        <div className="form-checkbox-container">
          <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handleFilterValueChange(e)
            }
            type="checkbox"
            name="filterByStatus"
            id={`filter-by-status-${notStarted}`}
            value={convertStatusToInputValue(notStarted)}
            defaultChecked={todoCtx.currentFilters.includes(
              convertStatusToInputValue(notStarted)
            )}
          />
          <label htmlFor={`filter-by-status-${notStarted}`}>Not Started</label>
        </div>
        <div className="form-checkbox-container">
          <input
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              handleFilterValueChange(e)
            }
            type="checkbox"
            name="filterByStatus"
            id={`filter-by-status-${suspended}`}
            value={convertStatusToInputValue(suspended)}
            defaultChecked={todoCtx.currentFilters.includes(
              convertStatusToInputValue(suspended)
            )}
          />
          <label htmlFor={`filter-by-status-${suspended}`}>Suspended</label>
        </div>
      </div>

      <div className={classes['sort-container']}>
        <h3 className={classes['sort-icon']}>
          <span>
            <FontAwesomeIcon icon={faCalendar} color="#f6efef" size="1x" />
          </span>
          Filter By Date:
        </h3>
        <div className="form-input-container">
          <label htmlFor="date-from" style={{ color: '#f6efef' }}>
            From:
          </label>
          <input
            type="date"
            name="date-from"
            id="date-from"
            style={{ backgroundColor: '#f6efef', color: 'rgb(50, 50, 50)' }}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setFilterByStartDate(e.target.value)
            }
          />
          {filterByStartDate &&
            filterByEndDate &&
            !isNotAfterDate(filterByStartDate, filterByEndDate) && (
              <p>Start date cannot be after end date</p>
            )}
          {formHasErrors && filterByStartDate && !filterByEndDate && (
            <p>Must specify end date if start date not empty</p>
          )}
        </div>
        <div className="form-input-container">
          <label htmlFor="date-to" style={{ color: '#f6efef' }}>
            To:
          </label>
          <input
            type="date"
            name="date-to"
            id="date-to"
            style={{ backgroundColor: '#f6efef', color: 'rgb(50, 50, 50)' }}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setFilterByEndDate(e.target.value)
            }
          />
          {filterByStartDate &&
            filterByEndDate &&
            !isNotBeforeDate(filterByEndDate, filterByStartDate) && (
              <p>End date cannot be before start date</p>
            )}
          {formHasErrors && filterByEndDate && !filterByStartDate && (
            <p>Must specify start date if end date not empty</p>
          )}
        </div>
      </div>

      <div className="form-controls-container">
        <button type="submit" className={classes['sort-filter-submit']}>
          APPLY
        </button>
      </div>
    </form>
  );
};

export default SortControls;
