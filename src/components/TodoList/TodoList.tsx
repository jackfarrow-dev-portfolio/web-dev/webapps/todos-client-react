import React, { useContext, useEffect } from 'react';
import { TodoContext } from '../../store/todo-context';
import TodoItem from '../TodoItem/TodoItem';
import { Todo } from '../../models/todo';
import TodoService from '../../services/todo-service';
import CategoryService from '../../services/category-service';
import TagService from '../../services/tag-service';
import { compareCharCodePoints } from '../../utils/sort-directions';
import classes from './TodoList.module.css';
import Chip from '../Chip/Chip';

const { getAllTodos } = TodoService;
const { fetchCategories } = CategoryService;
const { getTags } = TagService;

const TodoList: React.FC = () => {
  const todoCtx = useContext(TodoContext);

  useEffect(() => {
    const fetchTodos = async () => {
      const response = await getAllTodos();
      let todos: Todo[] = [];
      if (response.ok && response.data) {
        todos = response.data;
        todoCtx.handleSetTodos(
          todos.sort((a: Todo, b: Todo) =>
            compareCharCodePoints(a.title, b.title)
          )
        );
      } else {
      }
    };

    const fetchTodoCategories = async () => {
      const response = await fetchCategories();
      if (response.ok && response.data) {
        todoCtx.handleSetTodoCategories(response.data.categories);
      }
    };

    const fetchTodoTags = async () => {
      const response = await getTags();
      if (response.ok && response.data) {
        todoCtx.handleSetTodoTags(response.data);
      }
    };

    fetchTodos();
    fetchTodoCategories();
    fetchTodoTags();
  }, []);

  return (
    <div className={classes['todo-list']}>
      <div className={classes['chip-container']}>
        {todoCtx.currentFilters.map((f: string) => (
          <Chip
            key={`todo-filter-${f}`}
            isDismissable={false}
            title={f}
            type="information"
          />
        ))}
      </div>
      <div className={classes['chip-container']}>
        <Chip
          isDismissable={false}
          title={todoCtx.currentSortDirection}
          type="information"
        />
      </div>
      {todoCtx.currentDateFilterRange.length > 0 && (
        <div className={classes['chip-container']}>
          <Chip
            isDismissable={false}
            title={`From: ${todoCtx.currentDateFilterRange[0]}`}
            type="information"
          />
          <Chip
            isDismissable={false}
            title={`To: ${todoCtx.currentDateFilterRange[1]}`}
            type="information"
          />
        </div>
      )}
      {todoCtx.todoSlice.length > 0 && (
        <ul>
          {todoCtx.todoSlice.map((t: Todo) => (
            <TodoItem key={t.id} todo={t} />
          ))}
        </ul>
      )}
      {!todoCtx.todoSlice.length && <p>No Todos Shown!</p>}
    </div>
  );
};

export default TodoList;
