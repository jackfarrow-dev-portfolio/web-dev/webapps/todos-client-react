import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faX } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';
import classes from './Chip.module.css';

const Chip: React.FC<{
  isDismissable: boolean;
  title: string;
  clickHandler?: () => void;
  type: string;
}> = ({ clickHandler, isDismissable, title, type }) => {
  const [showHoverTitle, setShowHoverTitle] = useState<boolean>(false);
  return (
    <div
      className={classNames({
        [classes.chip]: true,
        [classes.information]: type === 'information',
        [classes.dismissable]: type === 'dismissable',
      })}
    >
      {isDismissable && (
        <span className={classes['chip-dismiss']} onClick={clickHandler}>
          <FontAwesomeIcon icon={faX} size="sm" />
        </span>
      )}
      {!isDismissable && <span className={classes['chip-dismiss']}>O</span>}
      <p
        onMouseEnter={() => setShowHoverTitle(true)}
        onMouseLeave={() => setShowHoverTitle(false)}
      >
        {title.length < 20 ? title : `${title.substring(0, 21)}...`}
      </p>
      {showHoverTitle && (
        <span className={classes['hover-title']}>{title}</span>
      )}
    </div>
  );
};

export default Chip;
