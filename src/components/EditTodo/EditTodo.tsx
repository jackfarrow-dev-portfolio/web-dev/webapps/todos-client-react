import { useContext, useEffect, useState } from 'react';
import { TrayContext } from '../../store/tray-context';
import { TodoContext } from '../../store/todo-context';
import { HttpStatusContext } from '../../store/http-status-context';
import {
  useTodoInput,
  useTodoTagInput,
  isNotEmpty,
  isNotFutureDate,
} from '../../hooks/use-todo-input';
import TodoService from '../../services/todo-service';
import { Todo } from '../../models/todo';
import { STATUS_FILTERS } from '../../utils/sort-directions';
import { toggleBodyOverflow } from '../../utils/dom-handlers';
import classNames from 'classnames';
import classes from './EditTodo.module.css';
import { Category } from '../../models/category';
import Tag from '../../models/tag';
import Chip from '../Chip/Chip';
import HttpResponse from '../../models/http-response';

const { complete, notStarted, inProgress, suspended } = STATUS_FILTERS;
const tagMax = 5;
const { createTodo, updateTodo } = TodoService;

const EditTodo: React.FC = () => {
  /* TODO: 2023-12-23 This component is LONG. Look into breaking it
    down into smaller component
  */
  const trayContext = useContext(TrayContext);
  const todoContext = useContext(TodoContext);
  const httpStatusCtx = useContext(HttpStatusContext);

  const cancelEdit = () => {
    trayContext.setModalMode({
      isEditMode: false,
      isModalOpen: false,
      isCreateMode: false,
      isDeleteMode: false,
    });
  };
  const [isLoading, setIsLoading] = useState(true);
  const [hasFormErrors, setHasFormErrors] = useState(false);
  const [remainingTagCount, setRemainingTagCount] = useState(
    todoContext.currentTodo && trayContext.isEditMode
      ? tagMax - todoContext.currentTodo!.tagsList.length
      : tagMax
  );

  const {
    inputValue: titleValue,
    isTouched: isTitleTouched,
    handleInputValueChange: handleTitleValueChange,
    handleInputBlur: handleTitleInputBlur,
    hasError: titleHasError,
  } = useTodoInput('', (inputValue) => isNotEmpty(inputValue));

  const {
    inputValue: descriptionValue,
    isTouched: isDescriptionTouched,
    handleInputValueChange: handleDescriptionValueChange,
    handleInputBlur: handleDescriptionInputBlur,
    hasError: descriptionHasError,
  } = useTodoInput('', (inputValue) => isNotEmpty(inputValue));

  const {
    inputValue: statusValue,
    handleInputValueChange: handleStatusValueChange,
  } = useTodoInput(
    todoContext.currentTodo
      ? todoContext.currentTodo.status
      : STATUS_FILTERS.notStarted,
    (inputValue) => isNotEmpty(inputValue)
  );

  const {
    inputValue: completeDateValue,
    isTouched: isCompleteDateTouched,
    handleInputValueChange: handleCompleteDateValueChange,
    handleInputBlur: handleCompleteDateInputBlur,
    hasError: completeDateHasError,
  } = useTodoInput(
    '',
    (inputValue) => isNotEmpty(inputValue) && isNotFutureDate(inputValue)
  );

  const initialCategoryId = todoContext.currentTodo
    ? todoContext.currentTodo.categoryId
    : '';

  const {
    inputValue: categoryValue,
    handleInputValueChange: handleCategoryValueChange,
  } = useTodoInput(initialCategoryId, () => true);

  const initialTagsListValue = trayContext.isEditMode
    ? todoContext.currentTodo!.tagsList
    : [];

  const {
    submitValue: tagsValue,
    handleTagsInputChange,
    handleChipDismissal,
    isValid: isTagsListValid,
  } = useTodoTagInput(
    initialTagsListValue,
    (tagsValue) => tagsValue.length < tagMax
  );

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setIsLoading(false);
    }, 125);

    return () => {
      clearTimeout(timeoutId);
    };
  }, []);

  /*
  TODO: 12-26-2023 Consider refactoring `submitHandler` to a separate file
  to reduce size of `EditTodo` component.
  */
  const submitHandler = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    // Maybe refactor the validation check to a separate function in THIS
    // COMPONENT, call new submit handler inside of it?
    if (
      !isTitleTouched() ||
      !isDescriptionTouched() ||
      (statusValue === complete && !isCompleteDateTouched()) ||
      titleHasError ||
      descriptionHasError ||
      (statusValue === complete && completeDateHasError)
    ) {
      setHasFormErrors(true);
    } else {
      const newTodo: Todo = {
        title: titleValue,
        description: descriptionValue,
        status: statusValue,
        createDate: trayContext.isCreateMode
          ? new Date(Date.now()).toISOString()
          : todoContext.currentTodo!.createDate,
        categoryId: categoryValue,
        tagsList: tagsValue,
        completeDate: statusValue === complete ? completeDateValue : null,
        lastUpdatedOn: new Date(Date.now()).toISOString(),
      };

      console.log('newTodo:', newTodo);
      const updateId = todoContext.currentTodo
        ? todoContext.currentTodo.id
        : '-1';

      let response: HttpResponse<Todo> = trayContext.isCreateMode
        ? await createTodo(newTodo)
        : await updateTodo({ ...newTodo, id: updateId });

      const status = {
        reqType: trayContext.isCreateMode ? 'POST' : 'PUT',
        reqStatusCode: response.status,
        reqUrl: response.url,
        isReqOk: response.ok,
        reqErrorMsg: response.message,
        todoTitle: newTodo.title,
        todoId: '',
        showHttpStatusMsg: true,
      };

      if (response.ok) {
        if (trayContext.isCreateMode) {
          todoContext.handleAddTodo(response.data!);
          status.todoId = response.data!.id!;
        } else if (trayContext.isEditMode) {
          if (todoContext.currentTodo && todoContext.currentTodo.id) {
            newTodo.id = todoContext.currentTodo.id;
            todoContext.handleUpdateTodo(newTodo);
          }
        }
      }

      todoContext.handleSetCurrentTodo(null);

      trayContext.setModalMode({
        isModalOpen: false,
        isCreateMode: false,
        isDeleteMode: false,
        isEditMode: false,
      });

      httpStatusCtx.handleSetHttpStatus(status);
    }
  };

  return (
    <form
      className={
        isLoading
          ? classNames({ [classes['edit-todo-form']]: true })
          : classNames({
              'fade-in': true,
              [classes['edit-todo-form']]: true,
            })
      }
      onSubmit={(e: React.FormEvent<HTMLFormElement>) => submitHandler(e)}
    >
      <h2 className="form-title">
        {trayContext.isEditMode ? `Edit` : `Create`} Todo
      </h2>
      <div className={classes['edit-form-content']}>
        <div className="form-input-container">
          <label htmlFor="edit-todo-title">
            Title <span className="required-field">(required)</span>
          </label>
          <input
            type="text"
            id="edit-todo-title"
            onBlur={handleTitleInputBlur}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              setHasFormErrors(false);
              handleTitleValueChange(e);
            }}
            defaultValue={
              todoContext.currentTodo && trayContext.isEditMode
                ? todoContext.currentTodo.title
                : ''
            }
          />
          <p>{titleHasError && 'Title Error Message'}</p>
          {hasFormErrors && !isTitleTouched() && <p>Title Untouched</p>}
        </div>
        <div className="form-input-container">
          <label htmlFor="edit-todo-description">
            Description <span className="required-field">(required)</span>
          </label>
          <textarea
            id="edit-todo-description"
            rows={5}
            onBlur={handleDescriptionInputBlur}
            onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
              setHasFormErrors(false);
              handleDescriptionValueChange(e);
            }}
            defaultValue={
              todoContext.currentTodo && trayContext.isEditMode
                ? todoContext.currentTodo.description
                : ''
            }
          />
          <p>{descriptionHasError && 'Description Error Message'}</p>
          {hasFormErrors && !isDescriptionTouched() && (
            <p>Description Untouched</p>
          )}
        </div>
        <div className="form-input-container">
          <label htmlFor="edit-todo-category">
            Category <span className="required-field">(required)</span>
          </label>
          <select
            name="edit-todo-category"
            onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>
              handleCategoryValueChange(e)
            }
            defaultValue={
              todoContext.currentTodo
                ? todoContext.currentTodo.categoryId
                : todoContext.todoCategories[0].categoryId
            }
          >
            {todoContext.todoCategories.map((c: Category) => (
              <option value={c.categoryId} key={c.categoryId}>
                {c.categoryName}
              </option>
            ))}
          </select>
        </div>
        <fieldset>
          <legend>Tags</legend>
          <div className="form-input-container">
            <label htmlFor="select-tag">Select a tag:</label>
            <select
              onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                if (isTagsListValid) {
                  handleTagsInputChange(e.target.value);
                  if (
                    !tagsValue.find((t: Tag) => t.tagName === e.target.value)
                  ) {
                    setRemainingTagCount((prevState) => (prevState -= 1));
                  }
                }
              }}
              onBlur={(e: React.ChangeEvent<HTMLSelectElement>) => {
                if (isTagsListValid) {
                  handleTagsInputChange(e.target.value);
                  if (
                    !tagsValue.find((t: Tag) => t.tagName === e.target.value)
                  ) {
                    setRemainingTagCount((prevState) => (prevState -= 1));
                  }
                }
              }}
            >
              <option disabled>Select...</option>
              {todoContext.todoTags.map((t: Tag) => (
                <option value={t.tagName} key={t.tagId}>
                  {t.tagName}
                </option>
              ))}
            </select>
          </div>
          <div className="form-input-container">
            <label htmlFor="add-new-tag">Or, add a new tag:</label>
            <input
              type="text"
              name="add-new-tag"
              onBlur={(e: React.ChangeEvent<HTMLInputElement>) => {
                if (isTagsListValid) {
                  handleTagsInputChange(e.target.value);
                  setRemainingTagCount((prevState) => (prevState -= 1));
                  e.target.value = '';
                }
              }}
            />
          </div>
          {isTagsListValid && (
            <p className={classes['tag-msg']}>
              You may add <span>{remainingTagCount}</span> more tags:
            </p>
          )}
          {!isTagsListValid && (
            <p className={classes['tag-msg']}>(Unable to add more tag)</p>
          )}
          {tagsValue.length > 0 && (
            <div className={classes['tags-list']}>
              {tagsValue &&
                tagsValue.length > 0 &&
                tagsValue.map((t: Tag) => (
                  <Chip
                    key={`${t.tagName}-${Math.floor(
                      Math.random() * (100000 - 1) + 1
                    )}`}
                    isDismissable={true}
                    type="dismissable"
                    title={t.tagName}
                    clickHandler={() => {
                      handleChipDismissal(t.tagName);
                      setRemainingTagCount((prevState) => (prevState += 1));
                    }}
                  />
                ))}
            </div>
          )}
        </fieldset>
        <fieldset>
          <legend>
            Status<span className="required-field">(required)</span>
          </legend>
          <div className="radio-container">
            <input
              type="radio"
              id="status-complete"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setHasFormErrors(false);
                handleStatusValueChange(e);
              }}
              name="status"
              value={complete}
              defaultChecked={
                todoContext.currentTodo &&
                todoContext.currentTodo.status === complete &&
                trayContext.isEditMode
                  ? true
                  : false
              }
            />
            <label htmlFor="status-complete">Complete</label>
          </div>
          <div className="radio-container">
            <input
              type="radio"
              id="status-not-started"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setHasFormErrors(false);
                handleStatusValueChange(e);
              }}
              defaultChecked={
                trayContext.isCreateMode ||
                (todoContext.currentTodo &&
                  todoContext.currentTodo.status === notStarted &&
                  trayContext.isEditMode)
                  ? true
                  : false
              }
              name="status"
              value={notStarted}
            />
            <label htmlFor="status-not-started">Not Started</label>
          </div>
          <div className="radio-container">
            <input
              type="radio"
              id="status-in-progress"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setHasFormErrors(false);
                handleStatusValueChange(e);
              }}
              name="status"
              value={inProgress}
              defaultChecked={
                todoContext.currentTodo &&
                todoContext.currentTodo.status === inProgress &&
                trayContext.isEditMode
                  ? true
                  : false
              }
            />
            <label htmlFor="status-complete">In Progress</label>
          </div>
          <div className="radio-container">
            <input
              type="radio"
              id="status-suspended"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setHasFormErrors(false);
                handleStatusValueChange(e);
              }}
              name="status"
              value={suspended}
              defaultChecked={
                todoContext.currentTodo &&
                todoContext.currentTodo.status === suspended &&
                trayContext.isEditMode
                  ? true
                  : false
              }
            />
            <label htmlFor="status-complete">Suspended</label>
          </div>
        </fieldset>

        {statusValue === complete && (
          <div className="form-input-container">
            <label htmlFor="complete-date">
              Completed On: <span className="required-field">(required)</span>
            </label>
            <input
              type="date"
              onBlur={handleCompleteDateInputBlur}
              onChange={handleCompleteDateValueChange}
            />
            <p>{completeDateHasError && 'Complete Date Error'}</p>
            {hasFormErrors && !isCompleteDateTouched() && (
              <p>Complete Date Untouched</p>
            )}
          </div>
        )}

        <div className="form-controls-container">
          <span className="form-controls-btn-span">
            <button
              type="button"
              className="cancel-button shadow-button"
              onClick={() => {
                toggleBodyOverflow(false);
                cancelEdit();
              }}
              style={{ fontWeight: 'bold' }}
            >
              CANCEL
            </button>
          </span>
          <span className="form-controls-btn-span">
            <button
              type="submit"
              className="confirm-button shadow-button"
              style={{ fontWeight: 'bold' }}
            >
              SAVE
            </button>
          </span>
        </div>
      </div>
    </form>
  );
};

export default EditTodo;
