import { useContext } from 'react';
import { TrayContext } from '../../store/tray-context';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAnglesLeft } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';
import classes from './Tray.module.css';

const Tray: React.FC<React.PropsWithChildren> = ({ children }) => {
  const trayCtx = useContext(TrayContext);

  return (
    <div
      className={
        trayCtx.isTrayOpen
          ? classNames({ [classes.tray]: true, [classes['tray-open']]: true })
          : classNames({ [classes.tray]: true, [classes['tray-closed']]: true })
      }
    >
      <button
        className={classes['tray-collapse']}
        onClick={() => {
          document
            .getElementsByTagName('body')[0]
            .classList.remove('overflow-y-hidden');
          trayCtx.handleToggleTray(false);
        }}
      >
        <FontAwesomeIcon icon={faAnglesLeft} size="xl" />
      </button>
      <h2 className={classes['sort-banner']}>Sort & Filter Todos</h2>
      {children}
    </div>
  );
};

export default Tray;
