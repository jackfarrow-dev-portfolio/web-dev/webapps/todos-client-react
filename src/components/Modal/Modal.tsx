import { useContext } from 'react';
import SortControls from '../SortControls/SortControls';
import { TrayContext } from '../../store/tray-context';
import Tray from '../Tray/Tray';
import EditTodo from '../EditTodo/EditTodo';
import DeleteConfirmation from '../DeleteTodo/DeleteTodo';
import classes from './Modal.module.css';

const Modal: React.FC<React.PropsWithChildren> = ({ children }) => {
  const trayCtx = useContext(TrayContext);

  return (
    <div id="modal" className={classes['modal']}>
      <Tray>
        <SortControls />
      </Tray>

      {(trayCtx.isModalOpen || trayCtx.isTrayOpen) && (
        <div className={classes['modal-overlay']}>
          {(trayCtx.isEditMode || trayCtx.isCreateMode) && <EditTodo />}
          {trayCtx.isDeleteMode && <DeleteConfirmation />}
        </div>
      )}
      <div className={classes['container']}>{children}</div>
    </div>
  );
};

export default Modal;
