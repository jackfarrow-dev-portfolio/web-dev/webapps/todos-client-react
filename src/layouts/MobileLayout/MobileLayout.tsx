import { useContext, useEffect, useState } from 'react';
import { HttpStatusContext } from '../../store/http-status-context';
import TodoList from '../../components/TodoList/TodoList';
import AppHeader from '../../components/AppHeader/AppHeader';
import Modal from '../../components/Modal/Modal';
import HttpStatusMessage from '../../components/HttpStatusMessage/HttpStatusMessage';
import classes from './MobileLayout.module.css';

const MobileLayout: React.FC = () => {
  const httpStatusCtx = useContext(HttpStatusContext);
  return (
    <div className={classes['mobile-layout']}>
      <Modal>
        <AppHeader title="Todo App" />
        {httpStatusCtx.showHttpStatusMsg && <HttpStatusMessage />}
        <TodoList />
      </Modal>
    </div>
  );
};

export default MobileLayout;
