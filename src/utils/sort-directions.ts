export const SORT_DIRS = {
  titleAZ: 'title A-Z',
  titleZA: 'title Z-A',
  dateAZ: 'date A-Z',
  dateZA: 'date Z-A',
  none: 'none',
};

export const STATUS_FILTERS: { [key: string]: string } = {
  complete: 'COMPLETED',
  notStarted: 'NOT_STARTED',
  inProgress: 'IN_PROGRESS',
  suspended: 'SUSPENDED',
};

export const compareStrings = (a: string, b: string) => {
  if (a.toLocaleLowerCase() < b.toLocaleLowerCase()) return -1;
  if (a.toLocaleLowerCase() > b.toLocaleLowerCase()) return 1;
  return 0;
};

export const compareStringsReverse = (a: string, b: string) => {
  if (a < b) return 1;
  if (a > b) return -1;
  return 0;
};

export const compareCharCodePoints = (a: string, b: string): number => {
  let aCodePointSum = 0;
  let bCodePointSum = 0;

  let x = 0;
  let y = 0;

  while (x < a.length && y < b.length) {
    aCodePointSum += a.charCodeAt(x);
    bCodePointSum += b.charCodeAt(y);
    x++;
    y++;
  }

  while (x < a.length) {
    aCodePointSum += a.charCodeAt(x);
    x++;
  }

  while (y < b.length) {
    bCodePointSum += b.charCodeAt(y);
    y++;
  }

  if (a > b) return 1;
  if (a < b) return -1;
  return 0;
};

export const compareDateStrings = (a: string, b: string): number => {
  try {
    const aAsEpochTime = Date.parse(a);
    const bAsEpochTime = Date.parse(b);

    if (aAsEpochTime > bAsEpochTime) return 1;
    if (aAsEpochTime < bAsEpochTime) return -1;
  } catch (err: any) {
    throw new Error('Unable to parse strings as Date objects');
  }
  return 0;
};

export const isDateWithinRange = (date: string, start: string, end: string) => {
  console.log(start);
  console.log(end);
  const dateAsEpochTime = Date.parse(date);
  return (
    dateAsEpochTime > Date.parse(start) && dateAsEpochTime <= Date.parse(end)
  );
};

export const transformFilter = (filter: string) => STATUS_FILTERS[filter];
