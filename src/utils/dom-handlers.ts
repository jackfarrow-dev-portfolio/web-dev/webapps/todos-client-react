export const toggleBodyOverflow = (hideOverflow: boolean): void => {
  let overflowClass = 'overflow-y-hidden';
  const target = document.getElementsByTagName('body')[0];
  if (!hideOverflow) {
    target.classList.remove(overflowClass);
    return;
  }
  target.classList.add(overflowClass);
};
