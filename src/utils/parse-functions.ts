import { STATUS_FILTERS } from './sort-directions';
import {
  faAppleWhole,
  faBaby,
  faBriefcase,
  faCalendarDays,
  faCarSide,
  faChampagneGlasses,
  faCheck,
  faChessKnight,
  faCircle,
  faDumbbell,
  faGraduationCap,
  faHouse,
  faKitchenSet,
  faPersonCane,
  faStaffSnake,
  faQuestion,
} from '@fortawesome/free-solid-svg-icons';

const { complete, inProgress, notStarted, suspended } = STATUS_FILTERS;

export const parseIconFromCategory = (categoryName: string) => {
  switch (categoryName) {
    // Business
    case 'Health & Fitness':
      return { icon: faDumbbell, color: '#2f4858' };
    case 'Learning & Development':
      return { icon: faGraduationCap, color: '#2f4858' };
    case 'Business':
      return { icon: faBriefcase, color: '#2f4858' };
    case 'Appointments':
      return { icon: faCalendarDays, color: '#2f4858' };
    case 'Automotive':
      return { icon: faCarSide, color: '#2f4858' };
    // Personal Care
    case 'Medical':
      return { icon: faStaffSnake, color: '#9a031e' };
    case 'Child Care':
      return { icon: faBaby, color: '#9a031e' };
    case 'Elder Care':
      return { faPersonCane, color: '#9a031e' };
    // Fun
    case 'Hobbies':
      return {
        icon: faChessKnight,
        color: '#33658a',
      };
    case 'Social':
      return { icon: faChampagneGlasses, color: '#33658a' };

    // Home/Food/Drink
    case 'Home & Garden':
      return { icon: faHouse, color: '#6a994e' };
    case 'Food & Drink':
      return { icon: faKitchenSet, color: '#6a994e' };
    case 'Nutrition':
      return { icon: faAppleWhole, color: '#6a994e' };
    default:
      return { icon: faQuestion, color: '#6a994e' };
  }
};

export const parseDisplayDate = (dateString: string): string => {
  const splitDate = dateString.split('-');
  splitDate[2] = splitDate[2].substring(0, 2);
  const [year, month, day] = splitDate;
  return `${year}-${month}-${day}`;
};

export const convertStatusToInputValue = (status: string): string => {
  const map = {
    [complete]: 'complete',
    [notStarted]: 'notStarted',
    [inProgress]: 'inProgress',
    [suspended]: 'suspended',
  };
  return map[status];
};

export const parseDisplayStatus = (status: string): string => {
  let displayStatus = '';
  switch (status) {
    case notStarted:
      displayStatus = 'Not Started';
      break;
    case inProgress:
      displayStatus = 'In Progress';
      break;
    case complete:
      displayStatus = 'Completed';
      break;
    case suspended:
      displayStatus = 'Suspended';
      break;
    default:
      displayStatus = 'Unknown Status';
  }
  return displayStatus;
};
