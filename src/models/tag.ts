export default interface Tag {
  tagId?: string;
  tagName: string;
}
