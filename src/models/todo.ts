import Tag from './tag';

export interface Todo {
  id?: string;
  title: string;
  description: string;
  status: string;
  createDate: string;
  categoryId: string;
  tagsList: Tag[];
  completeDate?: string | null;
  lastUpdatedOn?: string | null;
}
