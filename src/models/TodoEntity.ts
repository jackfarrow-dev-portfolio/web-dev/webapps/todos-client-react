import { Todo } from './todo';

export default class TodoEntity implements Todo {
  id: string;
  title: string;
  description: string;
  status: 'NOT_STARTED' | 'IN_PROGRESS' | 'COMPLETED' | 'SUSPENDED';
  createDate: string;

  constructor(
    id: string,
    title: string,
    description: string,
    status: 'NOT_STARTED' | 'IN_PROGRESS' | 'COMPLETED' | 'SUSPENDED',
    createDate: string
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.createDate = createDate;
    this.status = status;
  }

  // parseDateFromDateString(): { year: number; month: number; day: number } {
  //   const splitDate = this.createDate.split('-');
  //   splitDate[2] = splitDate[2].substring(0, 2);
  //   const [year, month, day] = splitDate;
  //   return {
  //     year: +year,
  //     month: +month,
  //     day: +day,
  //   };
  // }

  parseDisplayDate(): string {
    const splitDate = this.createDate.split('-');
    splitDate[2] = splitDate[2].substring(0, 2);
    const [year, month, day] = splitDate;
    return `${year}-${month}-${day}`;
  }

  parseDisplayStatus(): string {
    let displayStatus = '';
    switch (this.status) {
      case 'NOT_STARTED':
        displayStatus = 'Not Started';
        break;
      case 'IN_PROGRESS':
        displayStatus = 'In Progress';
        break;
      case 'COMPLETED':
        displayStatus = 'Completed';
        break;
      case 'SUSPENDED':
        displayStatus = 'Suspended';
        break;
      default:
        displayStatus = 'Unknown Status';
    }
    return displayStatus;
  }
}
