export default interface HttpResponse<T> {
  data?: T;
  status: number;
  ok: boolean;
  url: string;
  message?: string;
  timestamp?: string;
}
