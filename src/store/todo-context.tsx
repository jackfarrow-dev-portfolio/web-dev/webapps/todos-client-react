import React, { createContext, useReducer } from 'react';
import { Todo } from '../models/todo';
import todosReducer from './reducers/todos-reducer';
import { TODO_ACTIONS } from './reducers/todos-actions';
import { STATUS_FILTERS, SORT_DIRS } from '../utils/sort-directions';
import { convertStatusToInputValue } from '../utils/parse-functions';
import { Category } from '../models/category';
import Tag from '../models/tag';

const { complete, inProgress, notStarted, suspended } = STATUS_FILTERS;

export interface TodosState {
  todos: Todo[];
  todoSlice: Todo[];
  currentSortDirection: string;
  currentDateFilterRange: string[];
  currentTodo: Todo | null;
  currentFilters: string[];
  todoCategories: Category[];
  todoTags: Tag[];
}

export interface TodoContextInterface extends TodosState {
  handleAddTodo: (t: Todo) => void;
  handleRemoveTodo: (id: string) => void;
  handleUpdateTodo: (t: Todo) => void;
  handleSetTodos: (todos: Todo[]) => void;
  handleSortTodos: (sortDirection: string) => void;
  handleFilterTodos: (params: {
    filters: string[];
    sortDirection: string;
  }) => void;
  handleSetCurrentTodo: (t: Todo | null) => void;
  handleSetTodoCategories: (c: Category[]) => void;
  handleSetTodoTags: (t: Tag[]) => void;
  handleResetTodoFilters: () => void;
  handleFilterTodosByDateRange: (
    startDate: string,
    endDate: string,
    sortDirection: string
  ) => void;
}

export const initialTodoContext: TodoContextInterface = {
  todos: [],
  todoSlice: [],
  currentTodo: null,
  currentSortDirection: SORT_DIRS.titleAZ,
  currentDateFilterRange: [],
  currentFilters: [
    convertStatusToInputValue(complete),
    convertStatusToInputValue(inProgress),
    convertStatusToInputValue(notStarted),
    convertStatusToInputValue(suspended),
  ],
  todoCategories: [],
  todoTags: [],
  handleAddTodo: (_: Todo) => {},
  handleRemoveTodo: (_: string) => {},
  handleUpdateTodo: (_: Todo) => {},
  handleSetTodos: (_: Todo[]) => {},
  handleSortTodos: (_: string) => {},
  handleFilterTodos: (_: {}) => {},
  handleSetCurrentTodo: (_: Todo | null) => {},
  handleSetTodoCategories: (_: Category[]) => {},
  handleSetTodoTags: (_: Tag[]) => {},
  handleResetTodoFilters: () => {},
  handleFilterTodosByDateRange: (_: string, __: string, ___: string) => {},
};

export const TodoContext = createContext(initialTodoContext);

const TodoContextProvider: React.FC<React.PropsWithChildren> = ({
  children,
}) => {
  const [todosState, todosDispatch] = useReducer(
    todosReducer,
    initialTodoContext
  );

  const handleAddTodo = (todo: Todo) => {
    todosDispatch({ type: TODO_ACTIONS.add, payload: todo });
  };

  const handleRemoveTodo = (id: string) => {
    todosDispatch({ type: TODO_ACTIONS.remove, payload: id });
  };

  const handleUpdateTodo = (todo: Todo) => {
    todosDispatch({ type: TODO_ACTIONS.update, payload: todo });
  };

  const handleSetTodos = (todos: Todo[]) => {
    todosDispatch({ type: TODO_ACTIONS.set, payload: todos });
  };

  const handleSortTodos = (sortDirection: string) => {
    todosDispatch({ type: TODO_ACTIONS.sort, payload: sortDirection });
  };

  const handleFilterTodos = (params: {
    sortDirection: string;
    filters: string[];
  }) => {
    todosDispatch({ type: TODO_ACTIONS.filter, payload: params });
  };

  const handleFilterTodosByDateRange = (
    startDate: string,
    endDate: string,
    sortDirection: string
  ) => {
    todosDispatch({
      type: TODO_ACTIONS.filterByDateRange,
      payload: { startDate, endDate, sortDirection },
    });
  };

  const handleSetCurrentTodo = (todo: Todo | null) => {
    todosDispatch({ type: TODO_ACTIONS.setCurrent, payload: todo });
  };

  const handleSetTodoCategories = (categories: Category[]) => {
    todosDispatch({ type: TODO_ACTIONS.setCategories, payload: categories });
  };

  const handleSetTodoTags = (tags: Tag[]) => {
    todosDispatch({ type: TODO_ACTIONS.setTags, payload: tags });
  };

  const handleResetTodoFilters = () =>
    todosDispatch({ type: TODO_ACTIONS.resetFilters });

  const ctxValue: TodoContextInterface = {
    todos: todosState.todos,
    todoSlice: todosState.todoSlice,
    currentSortDirection: todosState.currentSortDirection,
    currentDateFilterRange: todosState.currentDateFilterRange,
    currentTodo: todosState.currentTodo,
    currentFilters: todosState.currentFilters,
    todoCategories: todosState.todoCategories,
    todoTags: todosState.todoTags,
    handleAddTodo,
    handleRemoveTodo,
    handleUpdateTodo,
    handleSetTodos,
    handleSortTodos,
    handleFilterTodos,
    handleFilterTodosByDateRange,
    handleSetCurrentTodo,
    handleSetTodoCategories,
    handleSetTodoTags,
    handleResetTodoFilters,
  };

  return (
    <TodoContext.Provider value={ctxValue}>{children}</TodoContext.Provider>
  );
};

export default TodoContextProvider;
