import React, { createContext, useReducer } from 'react';
import trayReducer from './reducers/tray-reducer';
import { TRAY_ACTIONS } from './reducers/tray-actions';

export interface TrayState {
  isTrayOpen: boolean;
  isModalOpen: boolean;
  isEditMode: boolean;
  isCreateMode: boolean;
  isDeleteMode: boolean;
}

export type ModalPayload = {
  isModalOpen: boolean;
  isEditMode: boolean;
  isCreateMode: boolean;
  isDeleteMode: boolean;
};

export interface TrayContextInterface extends TrayState {
  handleToggleTray: (isOpen: boolean) => void;
  setModalMode: (payload: ModalPayload) => void;
}

export const initialTrayContext: TrayContextInterface = {
  isTrayOpen: false,
  isModalOpen: false,
  isEditMode: false,
  isCreateMode: false,
  isDeleteMode: false,
  handleToggleTray: (_: boolean) => {},
  setModalMode: (_: ModalPayload) => {},
};

export const TrayContext = createContext(initialTrayContext);

const TrayContextProvider: React.FC<React.PropsWithChildren> = ({
  children,
}) => {
  const [trayState, trayDispatch] = useReducer(trayReducer, initialTrayContext);

  const handleToggleTray = (isOpen: boolean) => {
    trayDispatch({ type: TRAY_ACTIONS.toggleTray, payload: isOpen });
  };

  const setModalMode = (payload: ModalPayload) => {
    // console.log(`mode: ${mode}`);
    trayDispatch({ type: TRAY_ACTIONS.toggleModal, payload });
  };

  const ctxValue: TrayContextInterface = {
    isTrayOpen: trayState.isTrayOpen,
    isModalOpen: trayState.isModalOpen,
    isEditMode: trayState.isEditMode,
    isCreateMode: trayState.isCreateMode,
    isDeleteMode: trayState.isDeleteMode,
    handleToggleTray,
    setModalMode,
  };
  return (
    <TrayContext.Provider value={ctxValue}>{children}</TrayContext.Provider>
  );
};

export default TrayContextProvider;
