import { TrayState } from '../tray-context';
import { TrayAction, TRAY_ACTIONS } from './tray-actions';

const toggleTray = (action: TrayAction, prevState: TrayState): TrayState => {
  return {
    ...prevState,
    isTrayOpen: action.payload,
  };
};

const toggleModal = (action: TrayAction, prevState: TrayState): TrayState => {
  return {
    ...prevState,
    isEditMode: action.payload.isEditMode,
    isModalOpen: action.payload.isModalOpen,
    isCreateMode: action.payload.isCreateMode,
    isDeleteMode: action.payload.isDeleteMode,
  };
};

const trayReducer = (state: TrayState, action: TrayAction) => {
  if (action.type === TRAY_ACTIONS.toggleTray) {
    return toggleTray(action, state);
  }

  if (action.type === TRAY_ACTIONS.toggleModal) {
    return toggleModal(action, state);
  }
  return state;
};

export default trayReducer;
