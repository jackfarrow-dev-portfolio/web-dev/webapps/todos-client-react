import { HttpStatusState } from '../http-status-context';
import { HTTP_STATUS_ACTIONS } from './http-status-actions';
import { Action } from './action';

const { resetStatus, setStatus, setShowStatusMsg } = HTTP_STATUS_ACTIONS;

const setHttpStatus = (action: Action, prevState: HttpStatusState) => {
  return { ...prevState, ...action.payload };
};

const setShowHttpStatusMsg = (action: Action, prevState: HttpStatusState) => {
  return { ...prevState, showHttpStatusMsg: action.payload };
};

const resetHttpStatus = (prevState: HttpStatusState) => {
  const newState = {
    reqType: '',
    reqStatusCode: -1,
    reqUrl: '',
    isReqOk: false,
    reqErrorMsg: '',
    todoId: '',
    todoTitle: '',
    showHttpStatusMsg: false,
  };
  return { ...prevState, ...newState };
};

const httpStatusReducer = (state: HttpStatusState, action: Action) => {
  if (action.type === setStatus) {
    return setHttpStatus(action, state);
  }
  if (action.type === setShowStatusMsg) {
    return setShowHttpStatusMsg(action.payload, state);
  }
  if (action.type === resetStatus) {
    return resetHttpStatus(state);
  }
  return state;
};

export default httpStatusReducer;
