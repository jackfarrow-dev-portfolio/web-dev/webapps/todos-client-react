import { TodosState } from '../todo-context';
import { Todo } from '../../models/todo';
import { TODO_ACTIONS, TodoAction } from './todos-actions';
import {
  compareCharCodePoints,
  compareDateStrings,
  compareStrings,
  compareStringsReverse,
  SORT_DIRS,
  STATUS_FILTERS,
  transformFilter,
} from '../../utils/sort-directions';
import { convertStatusToInputValue } from '../../utils/parse-functions';

const { titleAZ, titleZA, dateAZ, dateZA } = SORT_DIRS;
const { complete, inProgress, suspended, notStarted } = STATUS_FILTERS;

export const setTodosList = (
  todos: Todo[],
  prevState: TodosState
): TodosState => {
  return {
    ...prevState,
    todos,
    todoSlice: todos,
  };
};

export const addTodoToList = (
  todo: Todo,
  prevState: TodosState
): TodosState => {
  const updatedTodos = [todo, ...prevState.todos];
  let updatedSlice = [...prevState.todoSlice];
  prevState.currentFilters.forEach((filter: string) => {
    if (todo.status !== transformFilter(filter)) {
      updatedSlice = [...prevState.todoSlice, todo];
    }
  });
  return {
    ...prevState,
    todos: updatedTodos,
    todoSlice: updatedSlice,
  };
};

export const addUpdatedTodoToList = (todo: Todo, prevState: TodosState) => {
  let foundIndex = prevState.todos.findIndex((t: Todo) => t.id === todo.id);
  let updatedTodos: Todo[] = [...prevState.todos];
  let updatedSlice: Todo[] = [...prevState.todoSlice];

  if (foundIndex >= 0) {
    updatedTodos[foundIndex] = todo;
  }
  let foundSliceIndex = prevState.todoSlice.findIndex(
    (t: Todo) => t.id === todo.id
  );
  if (foundSliceIndex >= 0) {
    updatedSlice[foundSliceIndex] = todo;
  }
  return {
    ...prevState,
    todos: updatedTodos,
    todoSlice: updatedSlice,
  };
};

export const removeTodoFromList = (
  todoId: string,
  prevState: TodosState
): TodosState => {
  if (prevState.todos.find((t: Todo) => t.id === todoId)) {
    const updatedTodos = prevState.todos.filter((t: Todo) => t.id !== todoId);
    const updatedSlice = prevState.todoSlice.filter(
      (t: Todo) => t.id !== todoId
    );
    return {
      ...prevState,
      todos: updatedTodos,
      todoSlice: updatedSlice,
    };
  }
  return prevState;
};

export const sortTodoSlice = (
  sortDirection: string,
  prevState: TodosState
): TodosState => {
  let sortedTodos: Todo[] = [];
  switch (sortDirection) {
    case titleAZ:
      sortedTodos = prevState.todoSlice.sort((a: Todo, b: Todo) =>
        compareCharCodePoints(a.title, b.title)
      );
      break;
    case titleZA:
      sortedTodos = prevState.todoSlice.sort((a: Todo, b: Todo) =>
        compareCharCodePoints(b.title, a.title)
      );
      break;
    case dateAZ:
      sortedTodos = prevState.todoSlice.sort((a: Todo, b: Todo) =>
        compareDateStrings(a.createDate, b.createDate)
      );
      break;
    case dateZA:
      sortedTodos = prevState.todoSlice.sort((a: Todo, b: Todo) =>
        compareDateStrings(b.createDate, a.createDate)
      );
      break;
  }
  return {
    ...prevState,
    todoSlice: sortedTodos,
    currentSortDirection: sortDirection,
  };
};

const filterTodoSliceByFilterParam = (
  params: { filters: string[]; sortDirection: string },
  state: TodosState
) => {
  const { sortDirection, filters } = params;

  // Return no todos if NO filters set
  if (filters.length === 0) {
    return { ...state, todoSlice: [], currentFilters: [] };
  } else {
    let nextSlice: Todo[] = [];
    let nextFilters: string[] = [];

    filters.forEach((filter: string) => {
      nextSlice = [
        ...nextSlice,
        ...state.todos.filter((t: Todo) => t.status === STATUS_FILTERS[filter]),
      ];

      nextFilters = [...nextFilters, filter];
    });
    const newSortedTodoSliceState = sortTodoSlice(sortDirection, {
      ...state,
      todoSlice: nextSlice,
      currentFilters: nextFilters,
    });

    return newSortedTodoSliceState;
  }
};

const filterTodoSliceByStartEndDate = (
  params: { startDate: string; endDate: string; sortDirection: string },
  state: TodosState
) => {
  const { startDate, endDate, sortDirection } = params;

  if (startDate.length === 0 && endDate.length === 0) {
    return { ...state, currentDateFilterRange: [] };
  }
  const startDateAsEpoch = Date.parse(startDate);
  const endDateAsEpoch = Date.parse(endDate) + 1000 * 60 * 60 * 24;

  let nextSlice: Todo[] = state.todos.filter(
    (t: Todo) =>
      Date.parse(t.createDate) >= startDateAsEpoch &&
      Date.parse(t.createDate) < endDateAsEpoch
  );

  const newSortedTodoSliceState = sortTodoSlice(sortDirection, {
    ...state,
    todoSlice: nextSlice,
    currentDateFilterRange: [startDate, endDate],
  });

  return newSortedTodoSliceState;
};

const setCurrentTodo = (todo: Todo | null, state: TodosState) => {
  return { ...state, currentTodo: todo };
};

function todosReducer(state: TodosState, action: TodoAction) {
  if (action.type === TODO_ACTIONS.add) {
    return addTodoToList(action.payload, state);
  }

  if (action.type === TODO_ACTIONS.remove) {
    return removeTodoFromList(action.payload, state);
  }

  if (action.type === TODO_ACTIONS.update) {
    return addUpdatedTodoToList(action.payload, state);
  }

  if (action.type === TODO_ACTIONS.set) {
    return { ...state, todos: action.payload, todoSlice: action.payload };
  }

  if (action.type === TODO_ACTIONS.sort) {
    return sortTodoSlice(action.payload, state);
  }

  if (action.type === TODO_ACTIONS.filter) {
    return filterTodoSliceByFilterParam(action.payload, state);
  }

  if (action.type === TODO_ACTIONS.filterByDateRange) {
    return filterTodoSliceByStartEndDate(action.payload, state);
  }

  if (action.type === TODO_ACTIONS.setCurrent) {
    return setCurrentTodo(action.payload, state);
  }

  if (action.type === TODO_ACTIONS.setCategories) {
    return { ...state, todoCategories: action.payload };
  }

  if (action.type === TODO_ACTIONS.setTags) {
    return { ...state, todoTags: action.payload };
  }

  if (action.type === TODO_ACTIONS.resetFilters) {
    return {
      ...state,
      currentFilters: [
        convertStatusToInputValue(complete),
        convertStatusToInputValue(inProgress),
        convertStatusToInputValue(suspended),
        convertStatusToInputValue(notStarted),
      ],
    };
  }
  return state;
}

export default todosReducer;
