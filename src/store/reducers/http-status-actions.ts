export const HTTP_STATUS_ACTIONS = {
  setStatus: '[Http Status] Set',
  setShowStatusMsg: '[Http Status] Set Show Status Msg',
  resetStatus: '[Http Status] Reset',
};
