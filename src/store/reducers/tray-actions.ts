export interface TrayAction {
  type: string;
  payload: any;
}

export const TRAY_ACTIONS = {
  toggleTray: '[Tray] Toggle Tray',
  toggleModal: '[Modal] Toggle Modal',
};
