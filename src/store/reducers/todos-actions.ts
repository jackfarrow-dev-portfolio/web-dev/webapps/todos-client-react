export interface TodoAction {
  type: string;
  payload?: any;
}

export const TODO_ACTIONS = {
  add: '[Todo] Add Todo',
  remove: '[Todo] Remove Todo',
  update: '[Todo] Update Todo',
  set: '[Todo] Set Todos',
  sort: '[Todo] Sort Todos',
  filter: '[Todo] Filter Todos',
  filterByDateRange: '[Todo] Filter By Date Range',
  resetFilters: '[Todo] Reset Filters',
  setCurrent: '[Todo] Set Current Todo',
  setCategories: '[Todo] Set Categories',
  setTags: '[Todo] Set Tags',
};
