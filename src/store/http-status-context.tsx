import { PropsWithChildren, createContext, useReducer } from 'react';
import httpStatusReducer from './reducers/http-status-reducer';
import { HTTP_STATUS_ACTIONS } from './reducers/http-status-actions';

export interface Status {}
export interface HttpStatusState {
  reqType: string;
  reqStatusCode: number;
  reqUrl: string;
  isReqOk: boolean;
  reqErrorMsg?: string;
  todoId: string;
  todoTitle: string;
  showHttpStatusMsg: boolean;
}

export interface HttpStatusContextInterface extends HttpStatusState {
  handleSetHttpStatus: (state: HttpStatusState) => void;
  handleShowHttpStatusMsg: (show: boolean) => void;
  handleResetHttpStatus: () => void;
}

export const initialHttpStatusContext: HttpStatusContextInterface = {
  reqType: '',
  reqStatusCode: -1,
  reqUrl: '',
  isReqOk: false,
  reqErrorMsg: '',
  todoId: '',
  todoTitle: '',
  showHttpStatusMsg: false,
  handleSetHttpStatus: (_: HttpStatusState) => {},
  handleShowHttpStatusMsg: (_: boolean) => {},
  handleResetHttpStatus: () => {},
};

export const HttpStatusContext = createContext(initialHttpStatusContext);

const HttpStatusContextProvider: React.FC<PropsWithChildren> = ({
  children,
}) => {
  const [httpState, httpStateDispatch] = useReducer(
    httpStatusReducer,
    initialHttpStatusContext
  );

  const handleSetHttpStatus = (httpState: HttpStatusState) => {
    httpStateDispatch({
      type: HTTP_STATUS_ACTIONS.setStatus,
      payload: httpState,
    });
  };

  const handleShowHttpStatusMsg = (showStatus: boolean) => {
    httpStateDispatch({
      type: HTTP_STATUS_ACTIONS.setShowStatusMsg,
      payload: showStatus,
    });
  };

  const handleResetHttpStatus = () => {
    httpStateDispatch({
      type: HTTP_STATUS_ACTIONS.resetStatus,
    });
  };

  const httpStatusCtxValue: HttpStatusContextInterface = {
    reqType: httpState.reqType,
    reqStatusCode: httpState.reqStatusCode,
    reqUrl: httpState.reqUrl,
    isReqOk: httpState.isReqOk,
    reqErrorMsg: httpState.reqErrorMsg || '',
    todoId: httpState.todoId,
    todoTitle: httpState.todoTitle,
    showHttpStatusMsg: httpState.showHttpStatusMsg,
    handleSetHttpStatus,
    handleShowHttpStatusMsg,
    handleResetHttpStatus,
  };
  return (
    <HttpStatusContext.Provider value={httpStatusCtxValue}>
      {children}
    </HttpStatusContext.Provider>
  );
};

export default HttpStatusContextProvider;
