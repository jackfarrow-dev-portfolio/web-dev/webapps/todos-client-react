import { useState } from 'react';
import { SORT_DIRS, STATUS_FILTERS } from '../utils/sort-directions';
import { convertStatusToInputValue } from '../utils/parse-functions';

const { complete, inProgress, notStarted, suspended } = STATUS_FILTERS;

const useSortFilterInput = () => {
  const [sortValue, setSortValue] = useState<string>(SORT_DIRS.titleAZ);
  const [filterValue, setFilterValue] = useState<string[]>([
    convertStatusToInputValue(complete),
    convertStatusToInputValue(inProgress),
    convertStatusToInputValue(notStarted),
    convertStatusToInputValue(suspended),
  ]);

  const handleSortValueChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setSortValue(event.target.getAttribute('value') || '');
  };

  const handleFilterValueChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const filter = event.target.getAttribute('value') || '';
    if (filterValue.indexOf(filter) === -1) {
      setFilterValue((prevState) => [filter, ...prevState]);
    } else {
      setFilterValue((prevState) => prevState.filter((f) => f !== filter));
    }
  };

  return {
    sortValue,
    handleSortValueChange,
    filterValue,
    handleFilterValueChange,
  };
};

export default useSortFilterInput;
