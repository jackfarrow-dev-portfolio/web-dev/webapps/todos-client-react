import { useState } from 'react';
import Tag from '../models/tag';

export const isNotEmpty = (val: string): boolean => {
  return val.trim().length > 0;
};

export const isNotFutureDate = (val: string): boolean => {
  try {
    const valAsDate = Date.parse(val);
    return valAsDate <= Date.now();
  } catch (err: any) {
    return false;
  }
};

export const useTodoInput = (
  defaultVal: string,
  validationFn: (val: string) => boolean
) => {
  const [inputValue, setInputValue] = useState<string>(defaultVal);
  const [didEdit, setDidEdit] = useState<boolean>(false);

  const handleInputValueChange = (
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >
  ) => {
    setInputValue(e.target.value || '');
  };

  const handleInputBlur = () => {
    setDidEdit(true);
  };

  let isInputValid = validationFn(inputValue);

  const isTouched = () => didEdit;

  return {
    inputValue,
    handleInputValueChange,
    handleInputBlur,
    hasError: didEdit && !isInputValid,
    isTouched,
  };
};

export const useTodoTagInput = (
  defaultTagsVal: Tag[],
  validationFn: (val: Tag[]) => boolean
) => {
  const [submitValue, setSubmitValue] = useState<Tag[]>(defaultTagsVal);

  let isValid = validationFn(submitValue);

  const handleTagsInputChange = (newVal: string) => {
    setSubmitValue((prevState) => {
      if (
        newVal.trim().length > 0 &&
        isValid &&
        !prevState.find(
          (t: Tag) =>
            t.tagName.toLowerCase().trim() === newVal.toLowerCase().trim()
        )
      ) {
        let newState = [{ tagName: newVal }, ...prevState];
        console.log(`newState:`, newState);
        return newState;
      }
      return prevState;
    });
  };

  const handleChipDismissal = (val: string) => {
    const trimmedVal = val.toLowerCase().trim();
    setSubmitValue((prevState) => {
      if (
        prevState.find(
          (t: Tag) => t.tagName.toLowerCase().trim() === trimmedVal
        )
      ) {
        return prevState.filter(
          (t: Tag) => t.tagName.toLowerCase().trim() !== trimmedVal
        );
      }
      return prevState;
    });
  };

  return {
    handleTagsInputChange,
    handleChipDismissal,
    submitValue,
    isValid,
  };
};
