import HttpStatusContextProvider from './store/http-status-context';
import TodoContextProvider from './store/todo-context';
import TrayContextProvider from './store/tray-context';
import MobileLayout from './layouts/MobileLayout/MobileLayout';
import './App.css';

function App() {
  return (
    <HttpStatusContextProvider>
      <TodoContextProvider>
        <TrayContextProvider>
          <MobileLayout />
        </TrayContextProvider>
      </TodoContextProvider>
    </HttpStatusContextProvider>
  );
}

export default App;
